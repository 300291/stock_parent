package com.itheima.service;

import com.itheima.stock.pojo.entity.SysUser;
import com.itheima.vo.resp.LoginReqVo;
import com.itheima.vo.resp.R;

//定义用户服务接口
public interface UserService {
    /**
     * 根据用户名称查询用户信息
     * @param userName
     * @return
     */
    SysUser findByUserName(String userName);

    /**
     * 用户登陆功能
     * @param vo
     * @return
     */
    R<LoginReqVo> login(LoginReqVo vo);
}
