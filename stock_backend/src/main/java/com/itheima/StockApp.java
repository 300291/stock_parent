package com.itheima;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//定义main启动类
@SpringBootApplication
@MapperScan("com.itheima.stock.mapper")//扫描持久层mapper接口，生成代理对象，并维护到springIOC的容器里
public class StockApp {
    public static void main(String[] args) {
        SpringApplication.run(StockApp.class, args);
    }
}