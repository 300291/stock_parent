package com.itheima.controller;

import com.itheima.service.UserService;
import com.itheima.stock.mapper.SysUserMapper;
import com.itheima.stock.pojo.entity.SysUser;
import com.itheima.vo.resp.LoginReqVo;
import com.itheima.vo.resp.R;
import com.itheima.vo.resp.ResponseCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

/**
 * 定义用户web层接口资源bean
 */
@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    private UserService userService;


    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 根据用户名称查询用户信息
     *
     * @param userName
     * @return
     */
    @GetMapping("/user/{userName}")
    public SysUser getUserByUserName(@PathVariable("userName") String userName) {
        return userService.findByUserName(userName);
    }

    /**
     * 用户登陆功能
     * @param vo
     * @return
     */
    @PostMapping("login")
    public R<LoginReqVo> login(@RequestBody LoginReqVo vo) {
        //1.判断参数是否合法
        boolean b = vo == null || StringUtils.isBlank(vo.getUsername()) || StringUtils.isBlank(vo.getPassword()) || StringUtils.isBlank(vo.getCode());
        if (b) {
            //b为true说明参数不合法，返回错误信息
            return R.error(ResponseCode.DATA_ERROR);//参数异常
        }
        //2.根据用户名去数据库查询用户信息，获取密码的密文
        SysUser dbUser = sysUserMapper.findUserInfoByUserName(vo.getUsername());
        if (dbUser==null){
            //用户不存在
            return R.error(ResponseCode.DATA_ERROR);
        }

        //3.调用密码匹配器匹配输入的明文密码和数据库的密文密码
        /**
         * vo.getPassword(),明文密码
         * dbUser.getPassword(),密文密码
         */
        boolean matches = passwordEncoder.matches(vo.getPassword(), dbUser.getPassword());
        if (!matches){
            //密码不匹配
            R.error(ResponseCode.USERNAME_OR_PASSWORD_ERROR);
        }

        //4.响应
        LoginReqVo respVo = new LoginReqVo();
        BeanUtils.copyProperties(dbUser,respVo);
        return R.ok(respVo);


    }


}
