package com.itheima.comfig;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootTest
public class TestPasswordEncoder {
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 测试密码加密
     */
    @Test
    public void test01() {
        String pwd = "123456";
        String encode = passwordEncoder.encode(pwd);
        System.out.println("encode = " + encode);
    }


    @Test
    public void test02() {
        String pwd = "123456";
        String encode = "$2a$10$PyyBP.qPtuqZhmbqIO4BcO.2MOCIMQloVhxoFYkKgyrY1qRT/TUUO";
        boolean matches = passwordEncoder.matches(pwd, encode);
        System.out.println(matches ? "密码匹配" : "密码不匹配");
    }
}
